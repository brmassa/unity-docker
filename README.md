Unity3D editor on Ubuntu Linux image which can be used for cloud building and testing.

# Unity Version
Use the proper tag to specify the version or omit it for the latest version. Examples:

* registry.gitlab.com/brmassa/unity-docker _(latest)_
* registry.gitlab.com/brmassa/unity-docker:5_5_0_B1
* registry.gitlab.com/brmassa/unity-docker:2017_1_1_B1
* registry.gitlab.com/brmassa/unity-docker:2017_2_0_F1

All versions that are listed in https://forum.unity.com/threads/unity-on-linux-release-notes-and-known-issues.350256 are included.

# Cloud Build (GitLab)
If you are using GitLab as your repository, you should 

```
unity:
    image: registry.gitlab.com/brmassa/unity-docker:latest
    variables:
        TARGET: "LINUX"
    script:
        - TIME=$(date +'%Y%m%d%H%M%S')

        # License
        - echo "$UNITY_LICENSE" > /root/.local/share/unity3d/Unity/Unity_lic.ulf

        # Build
        - mkdir -p /app/build/$TARGET
        - unity.sh "-batchmode -projectPath $(pwd) -buildLinuxUniversalPlayer /app/build/$TARGET -logFile -quit"

        # Compress
        - cd /app/build/
        - tar czf "$TARGET"-"$TIME".tar.gz /app/build/$TARGET
    artifacts:
        paths:
            - /app/build/"$TARGET"-"$TIME".tar.gz
```

# Wrapper
To facilitate, there is a small script called `unity.sh`, that includes all required arguments when calling Unity. You can use any of the Unity3D arguments (https://docs.unity3d.com/Manual/CommandLineArguments.html)

`unity.sh -projectPath=""`

The full command to call Unity3D is

`xvfb-run --error-file /var/log/xvfb_error.log --server-args="-screen 0 1024x768x24" /opt/Unity/Editor/Unity -quit -batchmode -projectPath=""`

# HELP
Super encorage you to help on the development. Contact me and contribute!