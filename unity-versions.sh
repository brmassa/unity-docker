#!/bin/bash

declare -A VERSIONS=(

[5_1_0_F3_2015082501]=http://download.unity3d.com/download_unity/unity-editor-5.1.0f3+2015082501_amd64.deb
[5_1_0_F3_2015090301]=http://download.unity3d.com/download_unity/unity-editor-5.1.0f3+2015090301_amd64.deb
[5_1_0_F3_2015091501]=http://download.unity3d.com/download_unity/unity-editor-5.1.0f3+2015091501_amd64.deb
[5_2_2_F1]=http://files.unity3d.com/levi/unity-editor-5.2.2f1+20151018_amd64.deb
[5_2_4_F1_2016031601]=http://download.unity3d.com/download_unity/linux/unity-editor-5.3.4f1+20160316_amd64.deb
[5_2_4_F1_2016031701]=http://download.unity3d.com/download_unity/linux/unity-editor-5.3.4f1+20160317_amd64.deb
[5_3_0_F4]=http://download.unity3d.com/download_unity/linux/unity-editor-5.3.0f4+20151218_amd64.deb
[5_3_1_F1]=http://download.unity3d.com/download_unity/linux/unity-editor-5.3.1f1+20160106_amd64.deb
[5_3_2_F1]=http://download.unity3d.com/download_unity/linux/unity-editor-5.3.2f1+20160208_amd64.deb
[5_3_3_F1]=http://download.unity3d.com/download_unity/linux/unity-editor-5.3.3f1+20160223_amd64.deb
[5_4_0_P1]=http://download.unity3d.com/download_unity/linux/unity-editor-5.4.0p1+20160810_amd64.deb
[5_5_3_F1]=http://beta.unity3d.com/download/a2454d41e248/unity-editor_amd64-5.5.3xf1Linux.deb
[5_6_0_F3]=http://beta.unity3d.com/download/8bc04e1c171e/unity-editor_amd64-5.6.0xf3Linux.deb
[5_6_2_F1]=http://beta.unity3d.com/download/ddd95e743b51/unity-editor_amd64-5.6.2xf1Linux.deb
[2017_1_0_F3]=http://beta.unity3d.com/download/061bcf22327f/unity-editor_amd64-2017.1.0xf3Linux.deb
[2017_1_2_F1]=http://beta.unity3d.com/download/7598623e6ed6/unity-editor_amd64-2017.1.2f1.deb
[2017_2_0_B6]=http://beta.unity3d.com/download/2b451a7da81d/unity-editor_amd64-2017.2.0xb6Linux.deb
[2017_2_0_F1]=http://beta.unity3d.com/download/ad31c9083c46/unity-editor_amd64-2017.2.0f1.deb
[2017_2_0_F3]=http://beta.unity3d.com/download/ee86734cf592/unity-editor_amd64-2017.2.0f3.deb

)

VERSION_LAST=2017_2_0_F1