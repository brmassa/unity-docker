FROM ubuntu

# Argument that can/should be given on building process. It will determine what Unity version should be installed
ARG UNITY_VERSION

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y \
      curl \
      debconf \
      gconf-service \
      lib32gcc1 \
      lib32stdc++6 \
      libasound2 \
      libc6 \
      libc6-i386 \
      libcairo2 \
      libcap2 \
      libcups2 \
      libdbus-1-3 \
      libexpat1 \
      libfontconfig1 \
      libfreetype6 \
      libgcc1 \
      libgconf-2-4 \
      libgdk-pixbuf2.0-0 \
      libgl1-mesa-glx \
      libglib2.0-0 \
      libglu1-mesa \
      libgtk2.0-0 \
      libnspr4 \
      libnss3 \
      libpango1.0-0 \
      libpq5 \
      libstdc++6 \
      libx11-6 \
      libxcomposite1 \
      libxcursor1 \
      libxdamage1 \
      libxext6 \
      libxfixes3 \
      libxi6 \
      libxrandr2 \
      libxrender1 \
      libxtst6 \
      lsb-release \
      npm \
      xdg-utils \
      xvfb \
      zlib1g

# Create needed folders
RUN mkdir -p /root/.cache/unity3d \
      && mkdir -p /root/.local/share/unity3d \
      && mkdir -p /root/.local/share/unity3d/Unity \
      && mkdir -p /root/.local/share/unity3d/Certificates

# Add the certificates that, for some reason, are not included in the Unity installation
ADD CACerts.pem /root/.local/share/unity3d/Certificates/CACerts.pem

# Create an empty file that MUTS be overwritten with the users' Unity License
RUN echo "" > /root/.local/share/unity3d/Unity/Unity_lic.ulf

# Use script to download Unity from the official site
ADD unity-versions.sh /app/unity-versions.sh
ADD unity-download.sh /app/unity-download.sh
ADD unity.sh /app/unity.sh
RUN chmod +x /app/unity-versions.sh && chmod +x /app/unity-download.sh && chmod +x /app/unity.sh

# Install Unity
RUN ["/bin/bash", "-c", "/app/unity-download.sh $UNITY_VERSION"]
RUN dpkg -i /app/unity_editor.deb && rm /app/unity_editor.deb
