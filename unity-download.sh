#!/bin/bash

. /app/unity-versions.sh

# If no version was given, use the latest
if [ $# -eq 0 ]
then
	UNITY_URL=${VERSIONS[$VERSION_LAST]}
else
	UNITY_URL=${VERSIONS[$1]}
fi

# Download
echo "Downloading Unity3D... $UNITY_URL"
curl $UNITY_URL > /app/unity_editor.deb
echo "Unity3D downloaded."